# bin2png

A simple tool to generate grayscale png representation of a binary file.

## Usage

Just run.

```
./bin2png --file <your-binary-file>
```

Example

```
./bin2png --file ~/.cargo/bin/rustc
```

Then check the `output.png` file.

For more options see `./bin2png --help`
