extern crate png;

use std::fs::File;
use std::io::{BufWriter, Read};
use std::convert::TryInto;
use std::path::PathBuf;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "bin2png")]
struct Opt {
    /// The path to the file to be opened.
    #[structopt(short, long, parse(from_os_str))]
    file: PathBuf,
    /// Optional output filename. The default name is
    /// `output.png`.
    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,
    /// Optional value to override the default width,
    /// for small binaries this can be helpful.
    #[structopt(short = "w", long)]
    width: Option<u32>,
}

const WIDTH: u32 = 2048;

fn main() {
    let opt = Opt::from_args();
    let width: u32;
    let output: PathBuf;
    let file = File::open(opt.file).unwrap();

    if let Some(w) = opt.width {
        width = w;
    } else {
        width = WIDTH;
    }

    if let Some(out) = opt.output {
        output = out;
    } else {
        output = PathBuf::from(r"output.png");
    }

    let out_file = File::create(output).unwrap();
    let size = file.metadata().unwrap().len();
    let height: u32 = get_height(size.try_into().unwrap(), width);

    let img_writer = BufWriter::new(out_file);
    let mut encoder = png::Encoder::new(img_writer, width, height);

    encoder.set_color(png::ColorType::Grayscale);
    let mut writer = encoder.write_header().unwrap();

    // Create a vector with the same points as the image and initialize
    // to `0`.
    let mut data = vec![0; (width * height).try_into().unwrap()];

    // Iterate over all bytes in file and set them in the data vector.
    for (bytes_count, b) in file.bytes().enumerate() {
        if let Some(e) = data.get_mut(bytes_count) {
            if let Ok(b) = b {
                *e = b;
            }
        }
    }

    writer.write_image_data(&data).unwrap();
}

// Calculates height based on the size of the binary.
// The png crate needs that the size of the data matches
// the size of the created image.
// This means that if `img_size % width > 0` then we need
// to add a new row to the image.
fn get_height(img_size: u32, width: u32) -> u32 {
    let mut rows: u32 = (img_size / width)
        .try_into().unwrap();
    if img_size % width > 0 {
        rows += 1;
    }
    rows
}


mod tests {
    #[test]
    fn set_image_size() {
        let size: u32 = 157982;
        assert_eq!(39, get_height(size));
        assert_eq!(1, get_height(1));
    }
}
